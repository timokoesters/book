# Contribute to Veloren
Everybody is welcome to fill Veloren with more creatures, help fixing bugs or just be active in the community.

While this chapter features more the official side of contributing, like guidelines you have to follow, tips and best practice will be in the chapter [Dev Tricks](../dev/README.md).
