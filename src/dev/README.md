# Dev Tricks

This chapter lists all kinds of patterns and code you might need while working on Veloren or compiling it from source.
It also features a list of common mistakes and how to detect and avoid them.
